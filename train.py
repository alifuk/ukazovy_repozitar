import cv2, os
import numpy as np
from tqdm import tqdm

images_path = r'C:\Users\3080 Ti\Downloads\celebs\img_align_celeba\img_align_celeba'

N_to_load = 10000

images = {}
for f in tqdm(os.listdir(images_path)[:N_to_load]):
    img_path = f"{images_path}/{f}"
    img = cv2.imread(img_path)
    images[f] = [img]

import pandas as pd
list_attr_celeba = pd.read_csv('list_attr_celeba.csv', nrows=N_to_load+1)

for index, row in list_attr_celeba.iterrows():
    image_id = row['image_id']
    if image_id in images:
        images[image_id].append(row['Male'])

X = np.array([ images[image_id][0] for image_id in images ])
Y = np.array([ images[image_id][1] for image_id in images ])

n, h , w = X.shape[:3]

X = X.reshape((n,h,w,3))
Y = Y.reshape((n,1))
Y = (Y +1) / 2

train_y_one_hot = keras.utils.to_categorical(Y,2)




import tensorflow.keras as keras
from keras.layers import BatchNormalization, Dropout
from tensorflow.keras import datasets
from keras.layers import Dense, Conv2D, MaxPooling2D,Flatten
from tensorflow.keras.models import Sequential






model = Sequential ()
model.add (Conv2D(16, (4, 4), activation= 'relu', padding= 'same', input_shape=(h, w, 3)))
model.add (BatchNormalization () )
model.add (MaxPooling2D ((2, 2)) )
for _ in range(3):
    model.add (Conv2D(16, (3, 3), activation= 'relu', padding= 'same'))
    model.add (MaxPooling2D ((2, 2)) )

model.add (BatchNormalization ())
model.add (Flatten())

model.add (Dense(2, activation= 'softmax' ))

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

model.summary()
history = model.fit(X, train_y_one_hot, epochs=10, batch_size=32, verbose=1)


model.save('is_man.h5')