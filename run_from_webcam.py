# import the opencv library
import cv2
from tensorflow import keras
model = keras.models.load_model('is_man.h5')
    
# define a video capture object
vid = cv2.VideoCapture(0)  
while(True):      
    # Capture the video frame
    # by frame
    ret, frame = vid.read()  
    # Display the resulting frame
    cv2.imshow('frame', frame)
    img = frame
    img = cv2.resize(img, (178,218))
    h , w, c = img.shape

    result = model.predict(img.reshape((1,h,w,c)))

    is_man = result[0][1] > 0.5
    if is_man:
        print('je to muž')
    else:
        print('je to žena')
      
    # the 'q' button is set as the
    # quitting button you may use any
    # desired button of your choice
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
  
# After the loop release the cap object
vid.release()
# Destroy all the windows
cv2.destroyAllWindows()